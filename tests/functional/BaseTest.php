<?php

namespace tests\functional;

use Codeception\AssertThrows;
use Codeception\TestCase\Test;
use Exception;
use Faker\Factory;
use FunctionalTester;
use LogicException;

/**
 * Base contains test cases for tesing api endpoint
 *
 * @see https://codeception.com/docs/modules/Yii2
 *
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
final class BaseTest extends Test
{
    use AssertThrows;

    /**
     * @var FunctionalTester
     */
    protected $tester;

    /**
     * Succeeded test cases for api
     *
     * @param array $page
     * @param mixed $expectedResponse
     *
     * @dataProvider baseTestProvider
     */
    public function testBase(array $page, $expectedResponse) : void
    {
        $this->tester->amOnPage($page);
        $actualResponse = $this->tester->grabPageAsJson();

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    /**
     * Failed test cases for api
     *
     * @param array $page
     *
     * @dataProvider baseTestShouldThrownExceptionProvider
     *
     * @throws Exception
     */
    public function testBaseShouldThrownException(array $page) : void
    {
        $this->assertThrows(LogicException::class, function () use ($page) {
            $this->tester->amOnPage($page);
        });
    }

    /**
     * @return array
     */
    public function baseTestProvider() : array
    {
        $faker = Factory::create();

        return [
            'bad params' => [
                'page' => [
                    'base/api',
                    'user' => [
                        'kfr',
                    ],
                    'platform' => [
                        'github',
                    ],
                ],
                'expectedResponse' => json_decode("Bad Request: Missing required parameters: users, platforms"),
            ],
            'empty users' => [
                'page' => [
                    'base/api',
                    'users' => [],
                    'platforms' => [
                        'github',
                    ],
                ],
                'expectedResponse' => json_decode("Bad Request: Missing required parameters: users"),
            ],
            'empty platforms' => [
                'page' => [
                    'base/api',
                    'users' => [
                        'yego87'
                    ],
                    'platforms' => [],
                ],
                'expectedResponse' => json_decode("Bad Request: Missing required parameters: platforms"),
            ],
            'unknown users' => [
                'page' => [
                    'base/api',
                    'users' => [
                        $faker->userName,
                    ],
                    'platforms' => [
                        'gitlab',
                    ],
                ],
                'expectedResponse' => [],
            ],
            'several platforms' => [
                'page' => [
                    'base/api',
                    'users' => [
                        'lluvigne'
                    ],
                    'platforms' => [
                        'github',
                        'gitlab',
                        'bitbucket'
                    ],
                ],
                'expectedResponse' => json_decode('[
                    {
                        "name": "lluvigne",
                        "platform": "github",
                        "total-rating": 2,
                        "repos": [],
                        "repo": [
                            {
                                "name": "ateam-docker-drupal-davinci",
                                "fork-count": 1,
                                "start-count": 0,
                                "watcher-count": 0,
                                "rating": 0.66666666666667
                            },
                            {
                                "name": "example_migration",
                                "fork-count": 2,
                                "start-count": 0,
                                "watcher-count": 0,
                                "rating": 1.3333333333333
                            },
                            {
                                "name": "d8cidemo",
                                "fork-count": 0,
                                "start-count": 0,
                                "watcher-count": 0,
                                "rating": 0
                            },
                            {
                                "name": "devdaysseville",
                                "fork-count": 0,
                                "start-count": 0,
                                "watcher-count": 0,
                                "rating": 0
                            }
                        ]
                    },
                    {
                        "name": "lluvigne",
                        "platform": "gitlab",
                        "total-rating": 0,
                        "repos": [],
                        "repo": [
                            {
                                "name": "website",
                                "fork-count": 0,
                                "start-count": 0,
                                "rating": 0
                            },
                            {
                                "name": "admin",
                                "fork-count": 0,
                                "start-count": 0,
                                "rating": 0
                            }
                        ]
                    }
                ]'),
            ],
            'several users' => [
                'page' => [
                    'base/api',
                    'users' => [
                        'FunnyGardener',
                        'sleightsou',
                    ],
                    'platforms' => [
                        'github'
                    ]
                ],
                'expectedResponse' => json_decode('[
                    {
                        "name": "FunnyGardener",
                        "platform": "github",
                        "total-rating": 0,
                        "repos": [],
                        "repo":[
                            {
                                "name": "Android-Homework",
                                "fork-count": 0,
                                "start-count": 0,
                                "watcher-count": 0,
                                "rating": 0
                            },
                            {
                                "name": "HTP-Homeworks",
                                "fork-count": 0,
                                "start-count": 0,
                                "watcher-count": 0,
                                "rating": 0
                            }
                        ]
                    },
                    {
                        "name": "sleightsou",
                        "platform": "github",
                        "total-rating": 0,
                        "repos": [],
                        "repo": [
                            {
                                "name": "JD2016",
                                "fork-count": 0,
                                "start-count": 0,
                                "watcher-count": 0,
                                "rating": 0
                            }
                        ]
                    }
                ]'),
            ],
            'mixed users' => [
                'page' => [
                    'base/api',
                    'users' => [
                        $faker->userName,
                        'yegor.veselov',
                    ],
                    'platforms' => [
                        'gitlab',
                    ],
                ],
                'expectedResponse' => json_decode('[
                    {
                        "name": "yegor.veselov",
                        "platform": "gitlab",
                        "total-rating": 0,
                        "repos": [],
                        "repo": [
                            {
                                "name": "test",
                                "fork-count": 0,
                                "start-count": 0,
                                "rating": 0
                            }
                        ]
                    }
                ]'),
            ],
        ];
    }

    /**
     * @return array
     */
    public function baseTestShouldThrownExceptionProvider() : array
    {
        $faker = Factory::create();

        return [
            'mixed platforms' => [
                'page' => [
                    'base/api',
                    'users' => [
                        $faker->userName,
                    ],
                    'platforms' => [
                        'gitlab',
                        $faker->word,
                    ],
                ],
            ],
            'unknown platforms' => [
                'page' => [
                    'base/api',
                    'users' => [
                        $faker->userName,
                    ],
                    'platforms' => [
                        $faker->word,
                    ],
                ],
            ],
        ];
    }
}