<?php

namespace tests\api;

use ApiTester;
use Codeception\TestCase\Test;
use yii\helpers\Json;

/**
 * Api test suite
 *
 * @group base
 *
 * @package tests\api
 */
final class BaseApiTest extends Test
{
    /**
     * @var string
     */
    public const ROUTE_BASE_API = 'base/api';

    /**
     * @var ApiTester
     */
    protected $tester;

    /**
     * @var string
     */
    protected $sourceData = 'tests/_data/data.json';

    /**
     * @var string
     */
    protected $resultsData = 'tests/_data/expected.json';

    /**
     *  @var array
     */
    protected $validCases = [
        'example api test'
    ];

    /**
     * Example API test
     *
     * @param array $data
     * @param $expectedResponse
     *
     * @dataProvider baseTestProvider
     */
    public function testBase(array $data, array $expectedResponse) : void
    {
        $this->tester->sendGET(static::ROUTE_BASE_API, $data);
        $actualResponse = $this->tester->grabJsonResponse();

        $this->assertSame($expectedResponse, $actualResponse);
    }

    /**
     * @return array
     */
    public function baseTestProvider() : array
    {
        $sourcesJsonData = Json::decode(file_get_contents($this->sourceData));
        $resultsJsonData = Json::decode(file_get_contents($this->resultsData));

        $data = [];

        foreach ($this->validCases as $caseIndex => $case) {

            if (isset($sourcesJsonData[$case], $resultsJsonData[$case])) {
                if (is_array($case)) {
                    $caseName = $caseIndex;
                } else {
                    $caseName = $case;
                }

                $source = $sourcesJsonData[$caseName];
                $result = $resultsJsonData[$caseName];

                $data[$caseName] = [
                    'data' => $source,
                    'expectedResult' => $result,
                ];
            }
        }

        return $data;
    }
}
