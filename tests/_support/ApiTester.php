<?php

use Codeception\Actor;
use yii\helpers\Json;


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
final class ApiTester extends Actor
{
    use _generated\ApiTesterActions;

   /**
    * Define custom actions here
    */

    /**
     * @return array
     */
    public function grabJsonResponse(): array
    {
        return Json::decode($this->grabResponse());
    }
}
