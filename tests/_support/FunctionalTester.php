<?php

use Codeception\Actor;
use Codeception\Lib\Friend;
use yii\helpers\Json;


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
final class FunctionalTester extends Actor
{
    use _generated\FunctionalTesterActions;

    /**
     * @return mixed
     */
    public function grabPageAsJson()
    {
        return json_decode($this->grabPageSource());
    }
}
