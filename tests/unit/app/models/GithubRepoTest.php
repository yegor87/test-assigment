<?php

namespace tests\unit\app\models;

use app\models\GithubRepo;
use Codeception\Test\Unit;
use Codeception\Util\Stub;
use Exception;
use Faker\Factory as FakerGenerator;

/**
 * GithubRepoTest contains test casess for github repo model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
final class GithubRepoTest extends Unit
{
    /**
     * Test case for counting repo rating
     *
     * @param string $name
     * @param int $forkCount
     * @param int $startCount
     * @param int $watcherCount
     * @param float $expectedResult
     *
     * @dataProvider ratingCountTestProvider
     */
    public function testRatingCount(string $name, int $forkCount, int $startCount, int $watcherCount, float $expectedResult) : void
    {
        $model = new GithubRepo($name, $forkCount, $startCount, $watcherCount);

        $actualResult = $model->getRating();

        $this->assertSame($expectedResult, $actualResult);
    }

    /**
     * Test case for repo model data serialization
     *
     * @param string $name
     * @param int $forkCount
     * @param int $startCount
     * @param int $watcherCount
     * @param float $stub
     * @param array $expectedResult
     *
     * @dataProvider dataTestProvider
     *
     * @throws Exception
     */
    public function testData(string $name, int $forkCount, int $startCount, int $watcherCount, float $stub, array $expectedResult) : void
    {
        $modelStub = Stub::construct(
            // create model instance
            new GithubRepo($name, $forkCount, $startCount, $watcherCount),
            // constructor params
            [$name, $forkCount, $startCount, $watcherCount],
            // stub for method
            ['getRating' => $stub]
        );

        $actualResult = $modelStub->getData();

        $this->assertSame($expectedResult, $actualResult);
    }

    /**
     * Test case for repo model __toString verification
     *
     * @param string $name
     * @param int $forkCount
     * @param int $startCount
     * @param int $watcherCount
     * @param string $expectedResult
     *
     * @dataProvider stringifyTestProvider
     */
    public function testStringify(string $name, int $forkCount, int $startCount, int $watcherCount, string $expectedResult) : void
    {
        $model = new GithubRepo($name, $forkCount, $startCount, $watcherCount);

        $actualResult = $model->__toString();

        $this->assertSame($expectedResult, $actualResult);
    }

    /**
     * @return array
     */
    public function ratingCountTestProvider() : array
    {
        $faker = FakerGenerator::create();

        return [
            'random data' => [
                'name' => $faker->userName,
                'forkCount' => $forkCount = $faker->randomNumber(),
                'startCount' => $startCount = $faker->randomNumber(),
                'watcherCount' => $watcherCount = $faker->randomNumber(),
                'expectedResult' => $this->calculateRating($forkCount, $startCount, $watcherCount),
            ],
        ];
    }

    /**
     * @return array
     */
    public function dataTestProvider() : array
    {
        $faker = FakerGenerator::create();

        return [
            'random data' => [
                'name' => $name = $faker->userName,
                'forkCount' => $forkCount = $faker->randomNumber(),
                'startCount' => $startCount = $faker->randomNumber(),
                'watcherCount' => $watcherCount = $faker->randomNumber(),
                'stub' => $stub = $this->calculateRating($forkCount, $startCount, $watcherCount),
                'expectedResult' => [
                    'name' => $name,
                    'fork-count' => $forkCount,
                    'start-count' => $startCount,
                    'watcher-count' => $watcherCount,
                    'rating' => $stub,
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function stringifyTestProvider() : array
    {
        $faker = FakerGenerator::create();

        return [
            'random data' => [
                'name' => $name = $faker->userName,
                'forkCount' => $forkCount = $faker->randomNumber(),
                'startCount' => $startCount = $faker->randomNumber(),
                'watcherCount' => $watcherCount = $faker->randomNumber(),
                'expectedResult' => sprintf(
                    "%-75s %4d ⇅ %4d ★ %4d 👁️",
                    $name,
                    $forkCount,
                    $startCount,
                    $watcherCount
                )
            ],
        ];
    }

    /**
     * @param int $forkCount
     * @param int $startCount
     * @param int $watcherCount
     *
     * @return float
     */
    protected function calculateRating(int $forkCount, int $startCount, int $watcherCount) : float
    {
        return (($forkCount * 2.0) + ($startCount / 2.0) + $watcherCount) / 3.0;
    }
}