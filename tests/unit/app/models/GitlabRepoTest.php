<?php

namespace tests\unit\app\models;

use app\models\GitlabRepo;
use Codeception\Test\Unit;
use Codeception\Util\Stub;
use Exception;
use Faker\Factory as FakerGenerator;

/**
 * GitlabRepoTest contains test casess for gitlab repo model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
final class GitlabRepoTest extends Unit
{
    /**
     * Test case for counting repo rating
     *
     * @param string $name
     * @param int $forkCount
     * @param int $startCount
     * @param float $expectedResult
     *
     * @dataProvider ratingCountTestProvider
     */
    public function testRatingCount(string $name, int $forkCount, int $startCount, float $expectedResult): void
    {
        $model = new GitlabRepo($name, $forkCount, $startCount);

        $actualResult = $model->getRating();

        $this->assertSame($expectedResult, $actualResult);
    }

    /**
     * Test case for repo model data serialization
     *
     * @param string $name
     * @param int $forkCount
     * @param int $startCount
     * @param float $stub
     * @param array $expectedResult
     *
     * @dataProvider dataTestProvider
     *
     * @throws Exception
     */
    public function testData(string $name, int $forkCount, int $startCount, float $stub, array $expectedResult): void
    {
        $modelStub = Stub::construct(
            // create model instance
            new GitlabRepo($name, $forkCount, $startCount),
            // constructor params
            [$name, $forkCount, $startCount],
            // stub for method
            ['getRating' => $stub]
        );

        $actualResult = $modelStub->getData();

        $this->assertSame($expectedResult, $actualResult);
    }

    /**
     * Test case for repo model __toString verification
     *
     * @param string $name
     * @param int $forkCount
     * @param int $startCount
     * @param string $expectedResult
     *
     * @dataProvider stringifyTestProvider
     */
    public function testStringify(string $name, int $forkCount, int $startCount, string $expectedResult): void
    {
        $model = new GitlabRepo($name, $forkCount, $startCount);

        $actualResult = $model->__toString();

        $this->assertSame($expectedResult, $actualResult);
    }

    /**
     * @return array
     */
    public function ratingCountTestProvider(): array
    {
        $faker = FakerGenerator::create();

        return [
            'random data' => [
                'name' => $faker->userName,
                'forkCount' => $forkCount = $faker->randomNumber(),
                'startCount' => $startCount = $faker->randomNumber(),
                'expectedResult' => $this->calculateRating($forkCount, $startCount),
            ],
        ];
    }

    /**
     * @return array
     */
    public function dataTestProvider(): array
    {
        $faker = FakerGenerator::create();

        return [
            'random data' => [
                'name' => $name = $faker->userName,
                'forkCount' => $forkCount = $faker->randomNumber(),
                'startCount' => $startCount = $faker->randomNumber(),
                'stub' => $stub = $this->calculateRating($forkCount, $startCount),
                'expectedResult' => [
                    'name' => $name,
                    'fork-count' => $forkCount,
                    'start-count' => $startCount,
                    'rating' => $stub,
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function stringifyTestProvider(): array
    {
        $faker = FakerGenerator::create();

        return [
            'random data' => [
                'name' => $name = $faker->userName,
                'forkCount' => $forkCount = $faker->randomNumber(),
                'startCount' => $startCount = $faker->randomNumber(),
                'expectedResult' => sprintf(
                    "%-75s %4d ⇅ %4d ★",
                    $name,
                    $forkCount,
                    $startCount
                )
            ],
        ];
    }

    /**
     * @param int $forkCount
     * @param int $startCount
     *
     * @return float
     */
    protected function calculateRating(int $forkCount, int $startCount): float
    {
        return (($forkCount * 2.0) + ($startCount / 2.0)) / 2.0;
    }
}