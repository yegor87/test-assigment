<?php

namespace tests\unit\app\models;

use app\models\GithubRepo;
use app\models\User;
use Codeception\AssertThrows;
use Codeception\Test\Unit;
use Codeception\Util\Stub;
use Exception;
use Faker\Factory as FakerFactory;
use LogicException;

/**
 * UserTest contains test casess for user model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
final class UserTest extends Unit
{
    use AssertThrows;

    /**
     * @var array
     */
    protected $userRepos = [];

    /**
     * @var float
     */
    protected $ratingStub = 0.0;

    /**
     * @inheritDoc
     *
     * @throws Exception
     */
    protected function _before()
    {
        parent::_before();

        $faker = FakerFactory::create();

        $this->ratingStub = $faker->randomFloat();

        $this->userRepos = [Stub::construct(
            // create model instance
            GithubRepo::class,
            // constructor params
            [$faker->name, $faker->randomNumber(), $faker->randomNumber(), $faker->randomNumber()],
            // stub for method
            ['getRating' => $this->ratingStub]
        )];
    }

    /**
     * Succeeded test case for adding repo models to user model
     *
     * IMPORTANT NOTE:
     * Should cover succeeded and failed suites
     *
     * @param string $identifier
     * @param string $name
     * @param string $platform
     *
     * @dataProvider userConstructorProvider
     */
    public function testAddingRepos(string $identifier, string $name, string $platform) : void
    {
        $expectedResult[] = $this->userRepos[0]->data;

        $model = new User($identifier, $name, $platform);

        $model->addRepos($this->userRepos);
        $actualResult = $model->getData()['repo'];

        $this->assertSame($expectedResult, $actualResult);
    }

    /**
     * Failed test case for adding repo models to user model
     *
     * @param string $identifier
     * @param string $name
     * @param string $platform
     *
     * @dataProvider userConstructorProvider
     *
     * @throws Exception
     */
    public function testAddingReposThrownException(string $identifier, string $name, string $platform) : void
    {
        $this->userRepos[] = null;

        $model = new User($identifier, $name, $platform);

        $this->assertThrows(LogicException::class, function () use ($model) {
            $model->addRepos($this->userRepos);
        });
    }

    /**
     * Test case for counting total user rating
     *
     * @param string $identifier
     * @param string $name
     * @param string $platform
     *
     * @dataProvider userConstructorProvider
     */
    public function testTotalRatingCount(string $identifier, string $name, string $platform) : void
    {
        $expectedResult = $this->calculateTotalRating();

        $model = new User($identifier, $name, $platform);
        $model->addRepos($this->userRepos);

        $actualResult = $model->getTotalRating();

        $this->assertSame($expectedResult, $actualResult);
    }

    /**
     * Test case for repo model data serialization
     *
     * @param string $identifier
     * @param string $platform
     * @param string $name
     *
     * @throws Exception
     *
     * @dataProvider userConstructorProvider
     *
     */
    public function testData(string $identifier, string $platform, string $name): void
    {
        $expectedResult = [
            'name' => $name,
            'platform' => $platform,
            'total-rating' => $this->ratingStub,
            'repos' => [],
            'repo' => [$this->userRepos[0]->getData()],
        ];

        $modelStub = Stub::construct(
            // create model instance
            User::class,
            // constructor params
            [$identifier, $name, $platform],
            // stub for method
            ['getTotalRating' => $this->ratingStub]
        );
        $modelStub->addRepos($this->userRepos);

        $actualResult = $modelStub->getData();

        $this->assertSame($expectedResult, $actualResult);
    }

    /**
     * Test case for repo model __toString verification
     *
     * @param string $identifier
     * @param string $name
     * @param string $platform
     *
     * @dataProvider userConstructorProvider
     */
    public function testStringify(string $identifier, string $name, string $platform): void
    {
        $expectedResult = $this->stringify($name = $this->userRepos[0]->name, $platform);

        $model = new User($identifier, $name, $platform);
        $model->addRepos($this->userRepos);

        $actualResult = $model->__toString();

        $this->assertSame($expectedResult, $actualResult);
    }

    /**
     * @return array
     */
    public function userConstructorProvider(): array
    {
        $faker = FakerFactory::create();

        return [
            'github platform' => [
                'identifier' => $faker->email,
                'name' => $faker->userName,
                'platform' => 'github',
            ],
        ];
    }

    /**
     * @return float
     */
    protected function calculateTotalRating() : float
    {
        $rating = 0.0;

        foreach ($this->userRepos as $repo) {
            $rating += $repo->getRating();
        }

        return $rating;
    }

    /**
     * @param string $name
     * @param string $platform
     *
     * @return string
     */
    protected function stringify(string $name, string $platform) : string
    {
        $stringify = sprintf(
            "%-75s %19d 🏆\n%'=98s\n",
            "{$name} ({$platform})",
            $this->userRepos[0]->rating,
            ""
        );

        foreach ($this->userRepos as $repo) {
            $stringify .= (string)$repo . "\n";
        }

        return $stringify;
    }
}