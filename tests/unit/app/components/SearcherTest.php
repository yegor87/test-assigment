<?php

namespace tests\unit\app\components;

use app\components\Factory;
use app\components\platforms\Github;
use app\components\platforms\Gitlab;
use app\components\Searcher;
use app\models\GithubRepo;
use app\models\GitlabRepo;
use app\models\User;
use Codeception\Test\Unit;
use Exception;
use Faker\Factory as FakerFactory;

/**
 * SearcherTest contains test casess for searcher component
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
final class SearcherTest extends Unit
{
    /**
     * Succeeded test case for searching via several platforms
     *
     * IMPORTANT NOTE:
     * Should cover succeeded and failed suites
     *
     * @group search
     *
     * @throws Exception
     */
    public function testSearcher(): void
    {
        $faker = FakerFactory::create();

        $userGithub_1 = new User($userName_1 = $faker->userName, $userName_1, Factory::PLATFORM_GITHUB);
        $userGithub_2 = new User($userName_2 = $faker->userName, $userName_2, Factory::PLATFORM_GITHUB);
        $userGitlab_1 = new User($userName_1, $userName_1, Factory::PLATFORM_GITLAB);

        $repoGitHub_1_1 = new GithubRepo($userName_1, $faker->randomNumber(), $faker->randomNumber(), $faker->randomNumber());
        $repoGitHub_2_1 = new GithubRepo($userName_2, $faker->randomNumber(), $faker->randomNumber(), $faker->randomNumber());
        $repoGitHub_2_2 = new GithubRepo($userName_2, $faker->randomNumber(), $faker->randomNumber(), $faker->randomNumber());
        $repoGitlab_1_1 = new GitlabRepo($userName_1, $faker->randomNumber(), $faker->randomNumber());

        $expectedResult = [$userGitlab_1, $userGithub_1, $userGithub_2];

        $platformGithubMock = $this->createMock(Github::class);
        $platformGithubMock->expects($this->exactly(2))
            ->method('findUserInfo')
            ->will($this->onConsecutiveCalls($userGithub_1, $userGithub_2));
        $platformGithubMock->expects($this->exactly(2))
            ->method('findUserRepos')
            ->will($this->onConsecutiveCalls([$repoGitHub_1_1], [$repoGitHub_2_1, $repoGitHub_2_2]));

        $platformGitlabMock = $this->createMock(Gitlab::class);
        $platformGitlabMock->expects($this->exactly(2))
            ->method('findUserInfo')
            ->will($this->onConsecutiveCalls(null, $userGitlab_1));
        $platformGitlabMock->expects($this->exactly(1))
            ->method('findUserRepos')
            ->will($this->onConsecutiveCalls([$repoGitlab_1_1]));

        $searchingPlatforms = [$platformGithubMock, $platformGitlabMock];
        $searchingUserNames = [$userName_1, $userName_2];

        $component = new Searcher();

        $actualResult = $component->search($searchingPlatforms, $searchingUserNames);

        $this->sort($expectedResult);
        $this->assertSame($expectedResult, $actualResult);
    }

    /**
     * Failed test case for searching via several platforms
     *
     * IMPORTANT NOTE:
     * Should cover succeeded and failed suites
     *
     * @group search
     *
     * @throws Exception
     */
    public function testSearcherShouldNotFond(): void
    {
        $faker = FakerFactory::create();

        $platformGithubMock = $this->createMock(Github::class);
        $platformGithubMock->expects($this->exactly(2))
            ->method('findUserInfo')
            ->will($this->onConsecutiveCalls(null, null));
        $platformGithubMock->expects($this->exactly(0))
            ->method('findUserRepos');

        $platformGitlabMock = $this->createMock(Gitlab::class);
        $platformGitlabMock->expects($this->exactly(2))
            ->method('findUserInfo')
            ->will($this->onConsecutiveCalls(null, null));
        $platformGitlabMock->expects($this->exactly(0))
            ->method('findUserRepos');

        $searchingPlatforms = [$platformGithubMock, $platformGitlabMock];
        $searchingUserNames = [$faker->userName, $faker->userName];

        $component = new Searcher();

        $actualResult = $component->search($searchingPlatforms, $searchingUserNames);

        $this->assertEmpty($actualResult);
    }

    /**
     * @param array $users
     */
    protected function sort(array &$users) : void
    {
        usort($users, function ($user_1, $user_2) {
            /** @var User $user_1 */
            /** @var User $user_2 */
            return $user_2->getTotalRating() - $user_1->getTotalRating();
        });
    }
}