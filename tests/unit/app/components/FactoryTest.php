<?php

namespace tests\unit\app\components;

use app\components\Factory;
use app\components\platforms\Bitbucket;
use app\components\platforms\Github;
use app\components\platforms\Gitlab;
use Codeception\AssertThrows;
use Codeception\Test\Unit;
use Faker\Factory as FakerGenerator;
use Exception;
use LogicException;

/**
 * FactoryTest contains test casess for factory component
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
final class FactoryTest extends Unit
{
    use AssertThrows;

    /**
     * Succeeded test case for creating platform component
     *
     * @param string $platformName
     * @param string $expectedClass
     *
     * @dataProvider createTestProvider
     */
    public function testCreate(string $platformName, string $expectedClass) : void
    {
        $component = new Factory();

        $actualResult = $component->create($platformName);

        $this->assertInstanceOf($expectedClass, $actualResult);
    }

    /**
     * Failed test case for creating platform component
     *
     * @param string $platformName
     *
     * @dataProvider createThrownExceptionTestProvider
     *
     * @throws Exception
     */
    public function testCreateThrownException(string $platformName) : void
    {
        $component = new Factory();

        $this->assertThrows(LogicException::class, function () use ($component, $platformName) {
            $component->create($platformName);
        });
    }

    /**
     * @return array
     */
    public function createTestProvider() : array
    {
        return [
            'github platform' => [
                'name' => 'github',
                'expectedResult' => Github::class,
            ],
            'gitlab platform' => [
                'name' => 'gitlab',
                'expectedResult' => Gitlab::class,
            ],
            'bitbucket platform' => [
                'name' => 'bitbucket',
                'expectedResult' => Bitbucket::class,
            ],
        ];
    }

    /**
     * @return array
     */
    public function createThrownExceptionTestProvider() : array
    {
        $faker = FakerGenerator::create();

        return [
            'empty platform name' => [
                'platformName' => '',
            ],
            'fake platform name' => [
                'platformName' => $faker->word,
            ],
        ];
    }
}